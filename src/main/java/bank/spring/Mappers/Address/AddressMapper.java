package bank.spring.Mappers.Address;

import bank.spring.model.Address.Address;
import bank.spring.model.Address.AddressDto;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {

    public Address addressMapped (AddressDto addressDto){
        Address address = new Address();
        address.setStreet(addressDto.getStreet());
        address.setBuildingNumber(addressDto.getBuildingNumber());
        address.setAppartmentNumber(addressDto.getAppartmentNumber());
        address.setZipCode(addressDto.getZipCode());
        address.setCity(addressDto.getCity());
        address.setAccountNumber(addressDto.getAccountNumber());
        return address;
    }

    public AddressDto addressDtoMapped (Address address){
        AddressDto addressDto = new AddressDto();
        addressDto.setStreet(address.getStreet());
        addressDto.setBuildingNumber(address.getBuildingNumber());
        addressDto.setAppartmentNumber(address.getAppartmentNumber());
        addressDto.setZipCode(address.getZipCode());
        addressDto.setCity(address.getCity());
        addressDto.setAccountNumber(address.getAccountNumber());
        return addressDto;
    }


}
