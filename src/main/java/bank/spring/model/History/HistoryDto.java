package bank.spring.model.History;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.sql.Date;

public class HistoryDto {

    @ApiModelProperty(value = "Id")
    @JsonProperty("id")
    private Long id;
    @ApiModelProperty(value = "Text")
    @JsonProperty("text")
    private String text;
    @ApiModelProperty(value = "History")
    @JsonProperty("history")
    @Enumerated(EnumType.ORDINAL)
    private HistoryType historyType;
    @ApiModelProperty(value = "Cash")
    @JsonProperty("cash")
    private double cash;
    @ApiModelProperty(value = "Date")
    @JsonProperty("date")
    private Date date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public HistoryType getHistoryType() {
        return historyType;
    }

    public void setHistoryType(HistoryType historyType) {
        this.historyType = historyType;
    }
}
