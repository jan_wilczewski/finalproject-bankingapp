package bank.spring.controller;

import bank.spring.Mappers.Address.AddressMapper;
import bank.spring.model.Address.Address;
import bank.spring.model.Address.AddressDto;
import bank.spring.model.Customer.Customer;
import bank.spring.repository.CustomerRepository;
import bank.spring.service.AddressService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.xml.ws.http.HTTPException;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;



@RunWith(MockitoJUnitRunner.class)
public class AddressControllerTest {

    @Mock
    public AddressService addressService;
    @Mock
    private AddressMapper addressMapper;
    @Mock
    private CustomerRepository customerRepository;
    @InjectMocks
    private AddressController testObj = new AddressController();


    @Test
    public void shouldReturnOK_whenAddAddressToDatabese(){

        AddressDto addressDto = new AddressDto();
        addressDto.setAccountNumber("123");

        Address address = new Address();

        when(addressMapper.addressMapped(addressDto)).thenReturn(address);

        ResponseEntity<Void> result = testObj.addAddressToDatabase(addressDto);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(addressMapper).addressMapped(addressDto);
        verify(addressService).addAddressToDatabase(address, "123");
        verifyNoMoreInteractions(addressMapper, addressService);
    }

    @Test
    public void shouldReturnNotFound_whenUserAddingToDatabaseThrownAnException(){

        AddressDto addressDto = new AddressDto();
        addressDto.setAccountNumber("123");

        Address address = new Address();

        when(addressMapper.addressMapped(addressDto)).thenReturn(address);
        Mockito.doThrow(new HTTPException(404)).when(addressService).addAddressToDatabase(address, "123");

        ResponseEntity<Void> result = testObj.addAddressToDatabase(addressDto);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(addressMapper).addressMapped(addressDto);
        verify(addressService).addAddressToDatabase(address, "123");
        verifyNoMoreInteractions(addressMapper, addressService);
    }

    @Test
    public void showAddressesTest(){

        String accountNumber = "123";
        Set<Address> addresses = new HashSet<>();

        Address address1 = new Address();
        addresses.add(address1);

        when(addressService.findAddressByAccountNumber("123")).thenReturn(addresses);

        ResponseEntity<Set<Address>> result = testObj.showAddresses(accountNumber);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(addresses);

    }

    @Test
    public void showAddressesTest_Error() {

        String accountNumber = "123";
        Set<Address> addresses = new HashSet<>();

        Address address1 = new Address();
        addresses.add(address1);

        Mockito.doThrow(new HTTPException(404)).when(addressService).findAddressByAccountNumber("123");

        ResponseEntity<Set<Address>> result = testObj.showAddresses(accountNumber);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(addressService).findAddressByAccountNumber(accountNumber);
        verifyNoMoreInteractions(addressService);
    }


    @Test
    public void addToAddress() {

        String street = "Graniczna";
        String modifiedValue = "city";
        String newValue = "Gdynia";

        when(addressService.update(street, modifiedValue, newValue)).thenReturn(true);

        ResponseEntity result = testObj.addToAddress(street, modifiedValue, newValue);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(addressService).update(street, modifiedValue, newValue);
        verifyNoMoreInteractions(addressService);
    }


    @Test
    public void addToAddress_Error() {

        String street = "Graniczna";
        String modifiedValue = "city";
        String newValue = "Gdynia";

        when(addressService.update(street, modifiedValue, newValue)).thenReturn(false);
        //Mockito.doThrow(new HTTPException(404)).when(addressService).update(street, modifiedValue, newValue);

        ResponseEntity result = testObj.addToAddress(street, modifiedValue, newValue);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        verify(addressService).update(street, modifiedValue, newValue);
        verifyNoMoreInteractions(addressService);

    }

}
