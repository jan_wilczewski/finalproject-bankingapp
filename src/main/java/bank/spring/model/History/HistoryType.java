package bank.spring.model.History;

public enum HistoryType {
    PAYMENT_ON_ACCOUNT, WITHDRAW, TRANSFER, RECEIVE_TRANSFER
}
