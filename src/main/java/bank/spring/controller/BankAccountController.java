package bank.spring.controller;

import bank.spring.Mappers.BankAccount.BankAccountMapper;
import bank.spring.Mappers.BankAccount.BankAccountMapper2;
import bank.spring.Mappers.BankAccount.BankAccountMapper3;
import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.BankAccount.BankAccountDto;
import bank.spring.model.BankAccount.NewBankAccountDto;
import bank.spring.model.History.History;
import bank.spring.repository.CustomerRepository;
import bank.spring.service.BankAccountService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.http.HTTPException;
import java.util.List;
import java.util.Map;

@RestController
public class BankAccountController {

    @Autowired
    private BankAccountService bankAccountService;
    @Autowired
    private BankAccountMapper bankAccountMapper;
    @Autowired
    private BankAccountMapper2 bankAccountMapper2;
    @Autowired
    private BankAccountMapper3 bankAccountMapper3;
    @Autowired
    private CustomerRepository customerRepository;


    @RequestMapping(value = "/Account", method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE },consumes=MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Zapisuje konto klienta do bazy danych.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Konto zostało zapisane w bazie.", response = Void.class),
            @ApiResponse(code = 400, message = "Niepoprawne dane wejściowe", response = Void.class),
            @ApiResponse(code = 500, message = "Nie można dodać konta do nie założonego profilu klienta z adresem.", response = Void.class) })
    public ResponseEntity<Void> addAccountToDatabase(@ApiParam(value = "Konto") @RequestBody NewBankAccountDto newBankAccountDto) {

        try {
            bankAccountService.addAccountToDatabase(bankAccountMapper3.accountMapped(newBankAccountDto), newBankAccountDto.getAccountNumber());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (HTTPException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }


    @RequestMapping(value = "/AccountByAccountNumber", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Szuka konta po numerze rachunku.", notes = "", response = List.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Konto znalezione", response = List.class),
            @ApiResponse(code = 401, message = "Nie znaleziono konta", response = Void.class) })
    public ResponseEntity<BankAccount> findAccountByAccountNumber(@ApiParam(value = "Szukany numer konta") @RequestParam("Account Number") String accountNumber) {

        try {
            BankAccount bankAccount = bankAccountService.findAccountByAccountNumber(accountNumber);
            BankAccountDto bankAccountDto = bankAccountMapper2.accountDtoMapped(bankAccount);
            return new ResponseEntity(bankAccountDto, HttpStatus.OK);
        }catch (HTTPException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }


    @RequestMapping(value = "/paymentOnAccount", method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Wpłata pieniędzy na konto.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Wpłata zaksięgowana. Bilans zaktualizowany", response = Void.class),
            @ApiResponse(code = 404, message = "Nie znaleziono konta", response = Void.class) })
    public ResponseEntity<Void> paymentOnAccount(@ApiParam(value = "Numer konta") @RequestParam("Account number") String accountNumber,
                                                 //@ApiParam(value = "Rodzaj operacji") @RequestParam("Operation type") HistoryType historyType,
                                                 @ApiParam(value = "Kwota") @RequestParam("cash") double cash) {

        try {
            boolean modifiedBalance = bankAccountService.paymentOnAccount(accountNumber,cash);
            return new ResponseEntity(modifiedBalance, HttpStatus.OK);
        }catch (HTTPException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }

    @RequestMapping(value = "/withdrawFromAccount", method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Wypłata pieniędzy z konta.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Wypłata zaksięgowana. Bilans zaktualizowany", response = Void.class),
            @ApiResponse(code = 400, message = "Brak wystarczających środków na koncie do wykonania operacji.", response = Void.class),
            @ApiResponse(code = 404, message = "Nie znaleziono konta", response = Void.class) })
    public ResponseEntity<Void> withdrawFromAccount(@ApiParam(value = "Numer konta") @RequestParam("Account number") String accountNumber,
                                                    @ApiParam(value = "Kwota") @RequestParam("cash") double cash) {

        try {
            boolean modifiedBalance = bankAccountService.withdrawFromAccount(accountNumber,cash);
            return new ResponseEntity(modifiedBalance, HttpStatus.OK);
        }catch (HTTPException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }

    @RequestMapping(value = "/transferToOtherAccount", method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Transfer pieniędzy z konta.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Transfer zaksięgowany. Bilans zaktualizowany", response = Void.class),
            @ApiResponse(code = 400, message = "Brak wystarczających środków na koncie do wykonania operacji.", response = Void.class),
            @ApiResponse(code = 404, message = "Nie znaleziono konta", response = Void.class) })
    public ResponseEntity<Void> transferToOtherAccount(@ApiParam(value = "Nazwa użytkowanika konta") @RequestParam("Account number") String accountNumber,
                                                       @ApiParam(value = "Kwota") @RequestParam("Money amount") double cash,
                                                       @ApiParam(value = "Nazwa konta odbiorcy") @RequestParam("Recipient account") String recipientAccountNumber) {

        try {
            boolean modifiedBalance = bankAccountService.transferToOtherAccount(accountNumber,cash,recipientAccountNumber);
            return new ResponseEntity(modifiedBalance, HttpStatus.OK);
        }catch (HTTPException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }



    @RequestMapping(value = "/History", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Szuke konto po numerze rachunku.", notes = "", response = List.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Konto znalezione", response = List.class),
            @ApiResponse(code = 401, message = "Nie znaleziono konta", response = Void.class) })
    public ResponseEntity<List<History>> showHistory(@ApiParam(value = "Szukany numer konta") @RequestParam("Account number") String accountNumber) {

        try {
            List<History> history = bankAccountService.showAccountHistoryByAccountNumber(accountNumber);
            return new ResponseEntity(history, HttpStatus.OK);
        }catch (HTTPException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }
}
