package bank.spring.model.Address;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Address")
public class AddressDto{


    @ApiModelProperty(value = "Street")
    @JsonProperty("street")
    private String street;
    @ApiModelProperty(value = "BuildingNumber")
    @JsonProperty("buildingNumber")
    private int buildingNumber;
    @ApiModelProperty(value = "AppartmentNumber")
    @JsonProperty("appartmentNumber")
    private int appartmentNumber;
    @ApiModelProperty(value = "ZipCode")
    @JsonProperty("zipCode")
    private String zipCode;
    @ApiModelProperty(value = "City")
    @JsonProperty("city")
    private String City;
    @ApiModelProperty(value = "Account Number")
    @JsonProperty("accountNumber")
    private String accountNumber;



    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(int buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public int getAppartmentNumber() {
        return appartmentNumber;
    }

    public void setAppartmentNumber(int appartmentNumber) {
        this.appartmentNumber = appartmentNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
