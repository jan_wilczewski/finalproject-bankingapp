package bank.spring.model.Message;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.sql.Date;

@ApiModel(description = "message")
public class MessageDto {


    @ApiModelProperty(value = "Id")
    @JsonProperty("id")
    private Long id;
    @ApiModelProperty(value = "Date")
    @JsonProperty("date")
    private Date date;
    @ApiModelProperty(value = "Text")
    @JsonProperty("text")
    private String text;
    @ApiModelProperty(value = "Type")
    @JsonProperty("messageType")
    @Enumerated(EnumType.ORDINAL)
    private MessageType messageType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

}
