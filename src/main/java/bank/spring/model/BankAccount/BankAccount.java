package bank.spring.model.BankAccount;

import bank.spring.model.Customer.Customer;
import bank.spring.model.History.History;

import javax.persistence.*;
import java.util.*;

@Entity(name = "bank_account")
public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column (name = "BANK_ID")
    private Long id;
    private double balance;
    private String accountNumber;

    @OneToOne
    private Customer customer;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "bank_history",
            joinColumns = {@JoinColumn(name = "BANK_ID")},
            inverseJoinColumns = {@JoinColumn(name = "HISTORY_ID")})
    private List<History> historyList = new LinkedList<>();




    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    public String getAccountNumber() {
        return accountNumber;
    }
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public List<History> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<History> historyList) {
        this.historyList = historyList;
    }
}
