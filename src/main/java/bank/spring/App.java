package bank.spring;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan(basePackages = "bank.spring")
@EnableJpaRepositories
@EnableTransactionManagement
public class App {

    public static void main(final String... args) {

        new SpringApplication(App.class).run(args);

    }

}
