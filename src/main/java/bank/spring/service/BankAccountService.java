package bank.spring.service;

import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.History.History;

import java.util.List;
import java.util.Map;

public interface BankAccountService {

    void addAccountToDatabase(BankAccount bankAccount, String accountNumber);

    BankAccount findAccountByAccountNumber(String accountNumber);

    boolean paymentOnAccount(String accountNumber, Double cash);
    boolean withdrawFromAccount(String accountNumber, Double cash);
    boolean transferToOtherAccount(String accountNumber, Double cash, String recipientName);

    List<History> showAccountHistoryByAccountNumber(String accountNumber);
}
