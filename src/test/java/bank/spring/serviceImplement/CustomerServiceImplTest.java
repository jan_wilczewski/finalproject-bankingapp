package bank.spring.serviceImplement;

import bank.spring.Mappers.Customer.NewCustomerMapper;
import bank.spring.model.Address.Address;
import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.Customer.Customer;
import bank.spring.repository.CustomerRepository;
import bank.spring.service.CustomerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import javax.xml.ws.http.HTTPException;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceImplTest {

    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private NewCustomerMapper newCustomerMapper;
    @InjectMocks
    private CustomerServiceImpl testObj = new CustomerServiceImpl();


    @Test
    public void shouldFindCustomerByUsername(){
        Customer customer = new Customer();
        BankAccount bankAccount = new BankAccount();
        Address address = new Address();
        Set<Address> addresses = new HashSet<>();
        addresses.add(address);

        customer.setUserName("jan");
        customer.setBankAccount(bankAccount);
        customer.setAddresses(addresses);

        when(customerRepository.findCustomerByUserName(anyString())).thenReturn(customer);
        Customer result = testObj.findCustomerByUserName("jan");

        assertThat(result).isNotNull();
        assertThat(result.getUserName()).isEqualTo("jan");
        verify(customerRepository).findCustomerByUserName("jan");
        verifyNoMoreInteractions(customerRepository);
    }

    @Test(expected = HTTPException.class)
    public void shouldNotFindCustomerByUsername(){
        Customer customer = new Customer();
        BankAccount bankAccount = new BankAccount();
        Address address = new Address();
        Set<Address> addresses = new HashSet<>();
        addresses.add(address);

        customer.setUserName("jan");
        customer.setBankAccount(bankAccount);
        customer.setAddresses(addresses);

        Customer result = testObj.findCustomerByUserName("adam");

        assertThat(result).isNull();
        verify(customerRepository).findCustomerByUserName("adam");
        verifyNoMoreInteractions(customerRepository);
    }

    @Test(expected = HTTPException.class)
    public void shouldThrowErrorWhenBankAccountNotUpdated() {
        Customer customer = new Customer();
        Address address = new Address();
        Set<Address> addresses = new HashSet<>();
        addresses.add(address);

        customer.setUserName("jan");
        customer.setAddresses(addresses);

        Mockito.doThrow(new HTTPException(204)).when(customerRepository).findCustomerByUserName(anyString());
        Customer result = testObj.findCustomerByUserName("jan");
    }

    @Test(expected = HTTPException.class)
    public void shouldThrowErrorWhenAddressNotUpdated() {
        Customer customer = new Customer();
        BankAccount bankAccount = new BankAccount();

        customer.setUserName("jan");
        customer.setBankAccount(bankAccount);

        Mockito.doThrow(new HTTPException(204)).when(customerRepository).findCustomerByUserName(anyString());
        Customer result = testObj.findCustomerByUserName("jan");
    }

    @Test
    public void shouldFindCustomerByAccountNumber(){
        Customer customer = new Customer();
        String accountNumber = "111";
        customer.setAccountNumber(accountNumber);

        when(customerRepository.findCustomerByAccountNumber(anyString())).thenReturn(customer);
        Customer result = testObj.findCustomerByAccountNumber(accountNumber);

        assertThat(result).isNotNull();
        assertThat(result.getAccountNumber()).isEqualTo(accountNumber);
        verify(customerRepository).findCustomerByAccountNumber(accountNumber);
        verifyNoMoreInteractions(customerRepository);
    }

    @Test(expected = HTTPException.class)
    public void shouldThrowErrorIfCustomerNotFoundByAccountNumber(){
        Customer customer = new Customer();
        String accountNumber = "111";
        customer.setAccountNumber(accountNumber);

        when(customerRepository.findCustomerByAccountNumber(anyString())).thenReturn(null);
        Customer result = testObj.findCustomerByAccountNumber("111");

        assertThat(result).isNull();
        verify(customerRepository).findCustomerByAccountNumber("111");
        verifyNoMoreInteractions(customerRepository);
    }

    @Test
    public void isAddingCustomerToDatabase(){
        Customer customer = new Customer();
        testObj.addCustomerToDatabase(customer);
        verify(customerRepository).save(customer);
        verifyNoMoreInteractions(customerRepository);
    }

    @Test(expected = HTTPException.class)
    public void isNotAddingCustomerToDatabase(){
        Customer customer = null;
        testObj.addCustomerToDatabase(customer);
    }

//    @Test
//    public void isRemovingCustomerFromDatabase(){
//        Customer customer = new Customer();
//        customer.setAccountNumber("111");
//
//        testObj.removeCustomerFromDatabaseByAccountNumber("111");
//
//        verify(customerRepository).removeCustomerByAccountNumber("111");
//        verifyNoMoreInteractions(customerRepository);
//    }

    @Test(expected = HTTPException.class)
    public void isNotRemovingCustomerFromDatabase(){
        Customer customer = new Customer();
        String accountNumber = "111";
        customer.setAccountNumber(accountNumber);

        testObj.removeCustomerFromDatabaseByAccountNumber("222");
        verify(customerRepository).removeCustomerByAccountNumber(accountNumber);
        verifyNoMoreInteractions(customerRepository);
    }



}
