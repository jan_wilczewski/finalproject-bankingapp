package bank.spring.Mappers.Customer;

import bank.spring.model.Customer.Customer;
import bank.spring.model.Customer.NewCustomerDto;
import org.springframework.stereotype.Component;

@Component
public class NewCustomerMapper {

    public Customer newCustomerMapped (NewCustomerDto newCustomerDto){
        Customer customer = new Customer();
        customer.setUserName(newCustomerDto.getUserName());
        customer.setPassword(newCustomerDto.getPassword());
        customer.setFirstName(newCustomerDto.getFirstName());
        customer.setLastName(newCustomerDto.getLastName());
        customer.setEmail(newCustomerDto.getEmail());
        customer.setPhone(newCustomerDto.getPhone());
        customer.setAccountNumber(newCustomerDto.getAccountNumber());
        return customer;
    }

    public NewCustomerDto newCustomerDtoMapped (Customer customer){
        NewCustomerDto newCustomerDto = new NewCustomerDto();
        newCustomerDto.setUserName(customer.getUserName());
        newCustomerDto.setPassword(customer.getPassword());
        newCustomerDto.setFirstName(customer.getFirstName());
        newCustomerDto.setLastName(customer.getLastName());
        newCustomerDto.setEmail(customer.getEmail());
        newCustomerDto.setPhone(customer.getPhone());
        newCustomerDto.setAccountNumber(customer.getAccountNumber());
        return newCustomerDto;
    }


}
