package bank.spring.Mappers.BankAccount;

import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.BankAccount.BankAccountDto;
import bank.spring.model.BankAccount.NewBankAccountDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class BankAccountMapper3Test {

    @InjectMocks
    private BankAccountMapper3 testObj = new BankAccountMapper3();

    @Test
    public void isAccountDtoMappedCorrectly(){
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(100);
        bankAccount.setAccountNumber("001");

        NewBankAccountDto result = testObj.newBankAccountDto(bankAccount);
        assertThat(result).isEqualToIgnoringGivenFields(bankAccount, "id", "customer", "historyList");
    }

    @Test
    public void  isAccountMappedCorrectly(){
        NewBankAccountDto newBankAccountDto = new NewBankAccountDto();
        newBankAccountDto.setBalance(100);
        newBankAccountDto.setAccountNumber("001");

        BankAccount result = testObj.accountMapped(newBankAccountDto);
        assertThat(result).isEqualToIgnoringGivenFields(newBankAccountDto, "id", "customer", "historyList");
    }


}
