package bank.spring.model.Customer;

import bank.spring.model.Address.Address;
import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.Message.Message;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "CUSTOMER")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CUSTOMER_ID")
    private Long id;

    @Column(unique = true)
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private int phone;
    @Column(unique = true, name = "ACCOUNT_NUMBER")
    private String accountNumber;
    private String role = "USER";

    @OneToOne(cascade = CascadeType.REMOVE)
    private BankAccount bankAccount;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CUSTOMER_ADDRESS",
                joinColumns = {@JoinColumn(name = "CUSTOMER_ID")},
                inverseJoinColumns = {@JoinColumn(name = "ADDRESS_ID")})
    private Set<Address> addresses;


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CUSTOMER_MESSAGE",
                joinColumns = {@JoinColumn(name = "CUSTOMER_ID")},
                inverseJoinColumns = {@JoinColumn(name = "MESSAGE_ID")})
    private List<Message> messages = new ArrayList<>();





    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public String getAccountNumber() {
        return accountNumber;
    }
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }
}
