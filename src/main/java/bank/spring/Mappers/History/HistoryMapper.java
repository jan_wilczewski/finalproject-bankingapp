package bank.spring.Mappers.History;


import bank.spring.model.History.History;
import bank.spring.model.History.HistoryDto;
import org.springframework.stereotype.Component;

@Component
public class HistoryMapper {

    public History historyMapped(HistoryDto historyDto){
        History history = new History();
        history.setHistoryType(historyDto.getHistoryType());
        history.setText(historyDto.getText());
        history.setCash(historyDto.getCash());
        history.setDate(historyDto.getDate());
        return history;
    }

    public HistoryDto historyDto (History history){
        HistoryDto historyDto = new HistoryDto();
        historyDto.setHistoryType(history.getHistoryType());
        historyDto.setText(history.getText());
        historyDto.setCash(history.getCash());
        historyDto.setDate(history.getDate());
        return historyDto;

    }
}
