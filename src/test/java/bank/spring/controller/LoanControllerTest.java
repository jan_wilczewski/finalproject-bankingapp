package bank.spring.controller;


import bank.spring.Mappers.Loan.LoanMapper;
import bank.spring.model.Loans.LoanType;
import bank.spring.service.LoansService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import javax.xml.ws.http.HTTPException;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class LoanControllerTest {

    @Mock
    LoansService loansService;
    @Mock
    LoanMapper loanMapper;
    @InjectMocks
    private LoanController testObj = new LoanController();

    @Test
    public void calculateTotalCostTest(){
        double principal = 100.00;
        double rate = LoanType.QUICK_CASH_LOAN.getInterestRate();
        int time = 2;
        LoanType type = LoanType.QUICK_CASH_LOAN;
        BigDecimal totalCost = BigDecimal.valueOf(121.0);

        when(loansService.getInterestYearly(principal, rate, time)).thenReturn(totalCost);
        ResponseEntity<BigDecimal> result = testObj.calculateTotalCost(principal, type, time);

        assertThat(result.getBody()).isEqualTo("121.0");
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(loansService).getInterestYearly(principal, rate, time);
        verifyNoMoreInteractions(loansService);
    }

    @Test
    public void calculateTotalCostTest_Error(){
        double principal = 100.00;
        double rate = LoanType.QUICK_CASH_LOAN.getInterestRate();
        int time = 2;
        LoanType type = LoanType.QUICK_CASH_LOAN;
        BigDecimal totalCost = BigDecimal.valueOf(121);

        Mockito.doThrow(new HTTPException(400)).when(loansService).getInterestYearly(principal, rate, time);
        ResponseEntity<BigDecimal> result = testObj.calculateTotalCost(principal, type, time);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        verify(loansService).getInterestYearly(principal, rate, time);
        verifyNoMoreInteractions(loansService);
    }

    @Test
    public void calculateMonthlyRateTest(){
        double principal = 100.00;
        double rate = LoanType.QUICK_CASH_LOAN.getInterestRate();
        int time = 2;
        LoanType type = LoanType.QUICK_CASH_LOAN;
        BigDecimal totalCost = BigDecimal.valueOf(10.08);

        when(loansService.getInterestMonthly(principal, rate, time)).thenReturn(totalCost);
        ResponseEntity<BigDecimal> result = testObj.calculateLoanMonthly(principal, type, time);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(loansService).getInterestMonthly(principal, rate, time);
        verifyNoMoreInteractions(loansService);
    }

    @Test
    public void calculateMonthlyRateTest_Error(){
        double principal = 100.00;
        double rate = LoanType.QUICK_CASH_LOAN.getInterestRate();
        int time = 2;
        LoanType type = LoanType.QUICK_CASH_LOAN;
        BigDecimal totalCost = BigDecimal.valueOf(10.08);

        Mockito.doThrow(new HTTPException(400)).when(loansService).getInterestMonthly(principal, rate, time);
        ResponseEntity<BigDecimal> result = testObj.calculateLoanMonthly(principal, type, time);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        verify(loansService).getInterestMonthly(principal, rate, time);
        verifyNoMoreInteractions(loansService);
    }
}
