package bank.spring.serviceImplement;

import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.Customer.Customer;
import bank.spring.model.History.History;
import bank.spring.model.History.HistoryType;
import bank.spring.repository.BankAccountRepository;
import bank.spring.repository.CustomerRepository;
import bank.spring.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.http.HTTPException;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class BankOperations implements BankAccountService {

    @Autowired
    private BankAccountRepository bankAccountRepository;
    @Autowired
    private CustomerRepository customerRepository;

    public void addAccountToDatabase(BankAccount bankAccount, String accountNumber){
        Customer customer = customerRepository.findCustomerByAccountNumber(accountNumber);
        BankAccount bankAccount0 = bankAccountRepository.findByAccountNumber(accountNumber);
        if (customer == null) {
            throw new HTTPException(404);
        }
        if (bankAccount0 != null){
            throw new HTTPException(400);
        }
        customer.setBankAccount(bankAccount);
        bankAccount.setCustomer(customer);
        bankAccountRepository.save(bankAccount);
    }


    @Override
    public BankAccount findAccountByAccountNumber (String accountNumber) {
        BankAccount bankAccount = bankAccountRepository.findByAccountNumber(accountNumber);
        if (bankAccount == null) {
            throw new HTTPException(404);
        }
        return bankAccount;
    }


    private History newHistory(HistoryType historyType, Double cash) {
        History history = new History();

        switch (historyType){
            case PAYMENT_ON_ACCOUNT:
                history.setText("Wpłacono na konto w PLN: ");
                break;
            case WITHDRAW:
                history.setText("Wypłacono z konta w PLN: ");
                break;
            case TRANSFER:
                history.setText("Przelano z konta w PLN: ");
                break;
            case RECEIVE_TRANSFER:
                history.setText("Otrzymano przelew w kwocie PLN: ");
        }

        history.setHistoryType(historyType);
        history.setCash(cash);
        history.setDate(new Date(System.currentTimeMillis()));
        return history;
    }


    @Override
    public boolean paymentOnAccount(String accountNumber, Double cash) {
        BankAccount originalBalance = bankAccountRepository.findByAccountNumber(accountNumber);
        if (originalBalance == null) {
            throw new HTTPException(404);
        }

        if (originalBalance != null){

            originalBalance.setBalance(originalBalance.getBalance() + cash);

            List<History> list = originalBalance.getHistoryList();
            list.add(newHistory(HistoryType.PAYMENT_ON_ACCOUNT, cash));
            originalBalance.setHistoryList(list);

            bankAccountRepository.save(originalBalance);
            return true;
        }
        return false;
    }


    @Override
    public boolean withdrawFromAccount(String accountNumber, Double cash) {
        BankAccount originalBalance = bankAccountRepository.findByAccountNumber(accountNumber);

        if (originalBalance == null) {
            throw new HTTPException(404);
        }
        if (originalBalance.getBalance() <= cash){
            throw new HTTPException(400);
        }

        if (originalBalance != null && originalBalance.getBalance() >= cash){
            originalBalance.setBalance(originalBalance.getBalance() - cash);

            List<History> list = originalBalance.getHistoryList();
            list.add(newHistory(HistoryType.WITHDRAW, cash));
            originalBalance.setHistoryList(list);

            bankAccountRepository.save(originalBalance);
            return true;
        }
        return false;
    }

    @Override
    public boolean transferToOtherAccount(String accountNumber, Double cash, String recipientAccount) {
        BankAccount originalBalance = bankAccountRepository.findByAccountNumber(accountNumber);
        BankAccount recipientBalance = bankAccountRepository.findByAccountNumber(recipientAccount);

        if (originalBalance == null) {
            throw new HTTPException(404);
        }
        if (originalBalance.getBalance() <= cash){
            throw new HTTPException(400);
        }

        if (originalBalance != null && originalBalance.getBalance() >= cash){
            originalBalance.setBalance(originalBalance.getBalance() - cash);
            recipientBalance.setBalance(recipientBalance.getBalance() + cash);

            List<History> listPayer = originalBalance.getHistoryList();
            List<History> listReceiver = recipientBalance.getHistoryList();
            listPayer.add(newHistory(HistoryType.TRANSFER, cash));
            listReceiver.add(newHistory(HistoryType.RECEIVE_TRANSFER, cash));
            originalBalance.setHistoryList(listPayer);
            recipientBalance.setHistoryList(listReceiver);

            bankAccountRepository.save(originalBalance);
            bankAccountRepository.save(recipientBalance);
            return true;
        }
        return false;
    }

    @Override
    public List<History> showAccountHistoryByAccountNumber(String accountNumber) {
        BankAccount account = bankAccountRepository.findByAccountNumber(accountNumber);
        if (account == null) {
            throw new HTTPException(404);
        }
        List<History> history = account.getHistoryList();
        return history;
    }
}
