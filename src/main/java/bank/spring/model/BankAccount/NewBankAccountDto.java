package bank.spring.model.BankAccount;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(description = "New Bank Account")
public class NewBankAccountDto {

    @ApiModelProperty(value = "Balance")
    @JsonProperty("balance")
    private double balance;

    @ApiModelProperty(value = "Account Number")
    @JsonProperty("accountNumber")
    private String accountNumber;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

}
