package bank.spring.Mappers.Customer;

import bank.spring.model.Address.Address;
import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.BankAccount.BankAccountDto;
import bank.spring.model.Customer.Customer;
import bank.spring.model.Customer.CustomerDto;
import bank.spring.model.Message.Message;
import javafx.scene.shape.Mesh;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class CustomerMapper4Test {

    @InjectMocks
    private CustomerMapper4 testObj = new CustomerMapper4();

    @Test
    public void isCustomerDtoMappedCorrectly(){
        BankAccount bankAccount = new BankAccount();
        List<Message> messages = new ArrayList<>();
        Set<Address> addresses = new HashSet<>();

        Customer customer = new Customer();
        customer.setUserName("aaa");
        customer.setPassword("bbb");
        customer.setFirstName("ccc");
        customer.setLastName("ddd");
        customer.setEmail("eee");
        customer.setPhone(123456789);
        customer.setAccountNumber("ggg");
        customer.setBankAccount(bankAccount);
        customer.setMessages(messages);
        customer.setAddresses(addresses);

        CustomerDto result = testObj.customerDtoMapped(customer);
        assertThat(result).isEqualToIgnoringGivenFields(customer, "id", "bankAccountDto");

    }

    @Test
    public void isCustomerMappedCorrectly(){
        BankAccountDto bankAccountDto = new BankAccountDto();
        List<Message> messages = new ArrayList<>();
        Set<Address> addresses = new HashSet<>();

        CustomerDto customerDto = new CustomerDto();
        customerDto.setUserName("aaa");
        customerDto.setPassword("bbb");
        customerDto.setFirstName("ccc");
        customerDto.setLastName("ddd");
        customerDto.setEmail("eee");
        customerDto.setPhone(123456789);
        customerDto.setAccountNumber("ggg");
        customerDto.setBankAccountDto(bankAccountDto);
        customerDto.setMessages(messages);
        customerDto.setAddresses(addresses);

        Customer result = testObj.customerMapped(customerDto);
        assertThat(result).isEqualToIgnoringGivenFields(customerDto, "id", "role", "bankAccount");

    }


}
