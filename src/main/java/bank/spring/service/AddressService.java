package bank.spring.service;

import bank.spring.model.Address.Address;

import java.util.Set;

public interface AddressService {


    void addAddressToDatabase(Address address, String accountNumber);
    boolean update(String accountNumber, String modifiedVariable, String newValue);

    Set<Address> findAddressByAccountNumber(String accountNumber);
    Address findByStreet(String street);
}
