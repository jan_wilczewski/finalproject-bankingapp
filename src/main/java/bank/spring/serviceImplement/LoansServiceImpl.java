package bank.spring.serviceImplement;

import bank.spring.service.LoansService;
import org.springframework.stereotype.Service;

import javax.xml.ws.http.HTTPException;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class LoansServiceImpl implements LoansService{
    @Override
    public BigDecimal getInterestYearly(double principal, double rate, int time) {
        if (principal == 0 || rate == 0 || time == 0){
            throw new HTTPException(400);
        }
        double capitalizedSum = 0;
        capitalizedSum = principal * Math.pow((1 + rate/100),time);
        BigDecimal bd = new BigDecimal(capitalizedSum).setScale(2, RoundingMode.HALF_EVEN);
        return bd;
    }

    @Override
    public BigDecimal getInterestMonthly(double principal, double rate, int time) {
        if (principal == 0 || rate == 0 || time == 0){
            throw new HTTPException(400);
        }
        double capitalizedSum = 0;
        capitalizedSum = (principal * Math.pow((1 + rate/100),time))/12;
        BigDecimal bd = new BigDecimal(capitalizedSum).setScale(2, RoundingMode.HALF_EVEN);
        return  bd;
    }
}
