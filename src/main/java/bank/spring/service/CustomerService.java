package bank.spring.service;

import bank.spring.model.Customer.Customer;
import bank.spring.model.Customer.NewCustomerDto;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

public interface CustomerService extends UserDetailsService{


    Customer findCustomerByUserName(String userName);
    void addCustomerToDatabase(Customer customer);
    List<NewCustomerDto> findCustomerByPassword(String password);
    Customer findCustomerByAccountNumber(String accountNumber);
    void removeCustomerFromDatabaseByAccountNumber(String accountNumber);
    boolean update(String userName, String modifiedVariable, String newValue);

    Customer findCustomer(String username);
    //UserDetails loadUserByUserName(String userName) throws UsernameNotFoundException;

//    void register(Customer customer);
//    Customer validateUser(LoginDto login);
}


