package bank.spring.controller;

import bank.spring.model.LoginDto;
import bank.spring.service.CustomerService;
import bank.spring.serviceImplement.TokenAuthenticationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.http.HTTPException;

@RestController
public class LoginController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TokenAuthenticationService tokenService;

    @RequestMapping(value = "/loginProcess", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Loguje pracownika.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Pracownik zalogowany", response = Void.class),
            @ApiResponse(code = 401, message = "Logowanie nieudane", response = Void.class),
            @ApiResponse(code = 500, message = "Nieprawidłowe dane, spróbuj ponownie.", response = Void.class) })
    public ResponseEntity<String> loginProcess(@ApiParam(value = "Dane logowania") @RequestBody LoginDto login) {

        try {
            String token = tokenService
                    .authenticate(new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword()));
            if (StringUtils.isEmpty(token)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            return new ResponseEntity<>(token, HttpStatus.OK);

        }catch (HTTPException e){
                return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }
}
