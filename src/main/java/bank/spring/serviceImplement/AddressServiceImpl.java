package bank.spring.serviceImplement;

import bank.spring.model.Address.Address;
import bank.spring.model.Customer.Customer;
import bank.spring.repository.AddressRepository;
import bank.spring.repository.CustomerRepository;
import bank.spring.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.http.HTTPException;
import java.util.Set;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private CustomerRepository customerRepository;

    public void addAddressToDatabase(Address address, String accountNumber) {
        Customer customer = customerRepository.findCustomerByAccountNumber(accountNumber);
        if (customer == null) {
            throw new HTTPException(404);
        }

        Set<Address> addresses = customer.getAddresses();
        addresses.add(address);
        customer.setAddresses(addresses);
        addressRepository.save(addresses);
    }

    @Override
    public Set<Address> findAddressByAccountNumber(String accountNumber) {

        Customer customer = customerRepository.findCustomerByAccountNumber(accountNumber);
        if (customer == null){
            throw new HTTPException(404);
        }
        Set<Address> addresses = customer.getAddresses();
        return addresses;
    }

    @Override
    public Address findByStreet(String street) {
        return addressRepository.findByStreet(street);
    }

    @Override
    public boolean update(String street, String modification, String newValue) {
        Address originalAddress = addressRepository.findByStreet(street);
        if (originalAddress != null){
            switch (modification) {
                case ("street"):
                    originalAddress.setStreet(newValue);
                    addressRepository.save(originalAddress);
                    return true;
                case ("buildingNumber"):
                    originalAddress.setBuildingNumber(Integer.parseInt(newValue));
                    addressRepository.save(originalAddress);
                    return true;
                case ("appartmentNumber"):
                    originalAddress.setAppartmentNumber(Integer.parseInt(newValue));
                    addressRepository.save(originalAddress);
                    return true;
                case ("zipCode"):
                    originalAddress.setZipCode(newValue);
                    addressRepository.save(originalAddress);
                    return true;
                case ("city"):
                    originalAddress.setCity(newValue);
                    addressRepository.save(originalAddress);
                    return true;
                case ("customerName"):
                    originalAddress.setAccountNumber(newValue);
                    addressRepository.save(originalAddress);
                    return true;
            }
        }
        return false;
    }

//    @Override
//    @Transactional
//    public void removeAddressByAccountNumber(String accountNumber) {
//
//        addressRepository.removeByAccountNumber(accountNumber);
//
//    }

}
