package bank.spring.model.Customer;

import bank.spring.model.Address.Address;
import bank.spring.model.Address.AddressDto;
import bank.spring.model.BankAccount.BankAccountDto;
import bank.spring.model.Message.Message;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import java.util.List;
import java.util.Set;

@ApiModel(description = "Customer")
public class CustomerDto {

    @ApiModelProperty(value = "Login")
    @JsonProperty("userName")
    @Length(min = 3)
    private String userName;
    @ApiModelProperty(value = "Password")
    @JsonProperty("password")
    @Length(min = 3)
    private String password;
    @ApiModelProperty(value = "Name")
    @JsonProperty("firstname")
    private String firstName;
    @ApiModelProperty(value = "Surname")
    @JsonProperty("lastname")
    private String lastName;
    @ApiModelProperty(value = "Email")
    @JsonProperty("email")
    private String email;
    @ApiModelProperty(value = "telefon")
    @JsonProperty("phone")
    private int phone;

    @ApiModelProperty(value = "Account Number")
    @JsonProperty("accountNumber")
    private String accountNumber;

    @ApiModelProperty(value = "BankAccount")
    @JsonProperty("bankAccount")
    private BankAccountDto bankAccountDto;




    @ApiModelProperty(value = "Addresses")
    @JsonProperty("addresses")
    private Set<Address> addresses;





    @ApiModelProperty(value = "Messages")
    @JsonProperty("messages")
    private List<Message> messages;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BankAccountDto getBankAccountDto() {
        return bankAccountDto;
    }

    public void setBankAccountDto(BankAccountDto bankAccountDto) {
        this.bankAccountDto = bankAccountDto;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

//    public AddressDto getAddressDto() {
//        return addressDto;
//    }
//
//    public void setAddressDto(AddressDto addressDto) {
//        this.addressDto = addressDto;
//    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }
}
