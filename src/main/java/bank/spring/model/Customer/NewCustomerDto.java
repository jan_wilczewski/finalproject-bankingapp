package bank.spring.model.Customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Customer")
public class NewCustomerDto {

    @ApiModelProperty(value = "Login")
    @JsonProperty("userName")
    private String userName;
    @ApiModelProperty(value = "Password")
    @JsonProperty("password")
    private String password;
    @ApiModelProperty(value = "Name")
    @JsonProperty("firstname")
    private String firstName;
    @ApiModelProperty(value = "Surname")
    @JsonProperty("lastname")
    private String lastName;
    @ApiModelProperty(value = "Email")
    @JsonProperty("email")
    private String email;
    @ApiModelProperty(value = "telefon")
    @JsonProperty("phone")
    private int phone;

    @ApiModelProperty(value = "Account Number")
    @JsonProperty("accountNumber")
    private String accountNumber;


    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }
}
