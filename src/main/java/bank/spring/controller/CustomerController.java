package bank.spring.controller;

import bank.spring.Mappers.Customer.CustomerMapper;
//import bank.spring.Mappers.CustomerMapper2;
//import bank.spring.Mappers.CustomerMapper3;
import bank.spring.Mappers.Customer.CustomerMapper4;
import bank.spring.Mappers.Customer.NewCustomerMapper;
import bank.spring.model.Customer.Customer;
import bank.spring.model.Customer.CustomerDto;
import bank.spring.model.Customer.NewCustomerDto;
import bank.spring.service.CustomerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.http.HTTPException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    public CustomerService customerService;
    @Autowired
    private CustomerMapper customerMapper;
    @Autowired
    private CustomerMapper4 customerMapper4;
    @Autowired
    private NewCustomerMapper newCustomerMapper;


    @RequestMapping(value = "/Customer", method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE },consumes=MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Zapisuje klienta do bazy danych.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Klient został zapisany w bazie.", response = Void.class),
            @ApiResponse(code = 400, message = "Niepoprawne dane wejściowe", response = Void.class) })
    public ResponseEntity<Void> addCustomerToDatabase(@ApiParam(value = "Rejestrowany user") @RequestBody NewCustomerDto newCustomerDto) {

        try {
            customerService.addCustomerToDatabase(newCustomerMapper.newCustomerMapped(newCustomerDto));
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (HTTPException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }


    @RequestMapping(value = "/FullCustomerDetailsWithAddressAndAccountByUsername", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Szuka klienta po nazwie użytkownika.", notes = "", response = List.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Klient znaleziony", response = List.class),
            @ApiResponse(code = 401, message = "Nie znaleziono klienta", response = Void.class),
            @ApiResponse(code = 500, message = "Ta opcja wyszukiwania dostępna jest tylko gdy klient ma uzupełnione dane adresowe i dane konta", response = Void.class)  })
    public ResponseEntity<List> findCustomerByUsername(@ApiParam(value = "Szukana nazwa użytkownika") @RequestParam("user name") String userName) {

        try {
            CustomerDto customerDto = customerMapper4.customerDtoMapped(customerService.findCustomerByUserName(userName));
            return new ResponseEntity(customerDto, HttpStatus.OK);
        }
        catch (HTTPException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }

    @RequestMapping(value = "/BasicCustomerDetailsByPassword", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Szuka klienta po haśle.", notes = "", response = List.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Klient znaleziony", response = List.class),
            @ApiResponse(code = 401, message = "Nie znaleziono klienta", response = Void.class) })
    public ResponseEntity<List<NewCustomerDto>> findByPassword(@ApiParam(value = "Szukane hasło") @RequestParam("password") String password) {

        try {
            List<NewCustomerDto>customers = customerService.findCustomerByPassword(password);
            return new ResponseEntity(customers, HttpStatus.OK);
        }
        catch (HTTPException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }

    @RequestMapping(value = "/Customer", method = RequestMethod.DELETE, produces = {
            MediaType.APPLICATION_JSON_VALUE },consumes=MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Usuwa klienta z bazy danych.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Klient został usunięty z bazy.", response = Void.class),
            @ApiResponse(code = 400, message = "Nie ma w bazie takiego klienta", response = Void.class) })
    public ResponseEntity<Void> removeCustomer(@ApiParam(value = "Usuwany klient") @RequestBody String accountNumber) {

        try {
            customerService.removeCustomerFromDatabaseByAccountNumber(accountNumber);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (HTTPException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }


    @RequestMapping(value = "/Customer", method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Zmienia wybrane dane personalne.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Dane zaktualizowane", response = Void.class),
            @ApiResponse(code = 404, message = "Nie znaleziono klienta", response = Void.class)})
    public ResponseEntity<Void> addToCustomer(@ApiParam(value = "Nazwa użytkowanika") @RequestParam("User Name") String userName,
                                              @ApiParam(value = "Modyfikowane pole") @RequestParam("Modified variable") String modification,
                                              @ApiParam(value = "Nowa wartość") @RequestParam("New value") String newValue){

        boolean modified = customerService.update(userName, modification, newValue);
        if (modified){
            return new ResponseEntity(modified, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
