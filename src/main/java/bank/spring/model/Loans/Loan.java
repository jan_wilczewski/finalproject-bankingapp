package bank.spring.model.Loans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by jan_w on 06.10.2017.
 */

@Entity
public class Loan {

    @Id
    @GeneratedValue
    private Long id;
    private double requestedValue;
    private int time;
    private LoanType type;

    public double getRequestedValue() {
        return requestedValue;
    }

    public void setRequestedValue(double requestedValue) {
        this.requestedValue = requestedValue;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public LoanType getType() {
        return type;
    }

    public void setType(LoanType type) {
        this.type = type;
    }

}
