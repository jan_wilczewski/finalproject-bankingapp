package bank.spring.serviceImplement;

import bank.spring.repository.LoansRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import javax.xml.ws.http.HTTPException;
import java.math.BigDecimal;
import java.sql.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.doubleThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class LoansServiceImplTest {

    @Mock
    private LoansRepository loansRepository;
    @InjectMocks
    private LoansServiceImpl testObj = new LoansServiceImpl();

    @Test
    public void shouldCalculateTotalLoanValue(){
        double principal = 100;
        double rate = 10;
        int time = 2;
        BigDecimal expected = new BigDecimal("121.00");

        BigDecimal result = testObj.getInterestYearly(principal, rate, time);
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldCalculateMonthlyRate(){
        double principal = 100;
        double rate = 10;
        int time = 2;
        BigDecimal expected = new BigDecimal("10.08");

        BigDecimal result = testObj.getInterestMonthly(principal, rate, time);
        assertThat(result).isEqualTo(expected);
    }

    @Test(expected = HTTPException.class)
    public void shouldThrowErrorForTotalLoanCostWhenPrincipalIsZero() {
        double principal = 0;
        double rate = 10;
        int time = 2;

        testObj.getInterestYearly(principal, rate, time);
    }

    @Test(expected = HTTPException.class)
    public void shouldThrowErrorForTotalLoanCostWhenRatelIsZero() {
        double principal = 100;
        double rate = 0;
        int time = 2;

        testObj.getInterestYearly(principal, rate, time);
    }

    @Test(expected = HTTPException.class)
    public void shouldThrowErrorForTotallyLoanCostWhenTimeIsZero() {
        double principal = 100;
        double rate = 10;
        int time = 0;

        testObj.getInterestYearly(principal, rate, time);
    }

    @Test(expected = HTTPException.class)
    public void shouldThrowErrorForMonthlyLoanCostWhenPrincipalIsZero() {
        double principal = 0;
        double rate = 10;
        int time = 2;

        testObj.getInterestMonthly(principal, rate, time);
    }

    @Test(expected = HTTPException.class)
    public void shouldThrowErrorForMonthlyLoanCostWhenRatelIsZero() {
        double principal = 100;
        double rate = 0;
        int time = 2;

        testObj.getInterestMonthly(principal, rate, time);
    }

    @Test(expected = HTTPException.class)
    public void shouldThrowErrorForMonthlyLoanCostWhenTimeIsZero() {
        double principal = 100;
        double rate = 10;
        int time = 0;

        testObj.getInterestMonthly(principal, rate, time);
    }
}
