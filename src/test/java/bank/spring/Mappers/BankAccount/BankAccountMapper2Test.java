package bank.spring.Mappers.BankAccount;

import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.BankAccount.BankAccountDto;
import bank.spring.model.History.History;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Component
public class BankAccountMapper2Test {

    @InjectMocks
    private BankAccountMapper2 testObj = new BankAccountMapper2();

    @Test
    public void isAccountDtoMappedCorrectly(){
        History history = new History();
        List<History> listHist = new ArrayList<>();
        listHist.add(history);

        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(100);
        bankAccount.setAccountNumber("001");
        bankAccount.setHistoryList(listHist);

        BankAccountDto result = testObj.accountDtoMapped(bankAccount);
        assertThat(result).isEqualToIgnoringGivenFields(bankAccount, "id");
    }

    @Test
    public void  isAccountMappedCorrectly(){
        History history = new History();
        List<History> listHist = new ArrayList<>();
        listHist.add(history);

        BankAccountDto bankAccountDto = new BankAccountDto();
        bankAccountDto.setBalance(100);
        bankAccountDto.setAccountNumber("001");
        bankAccountDto.setHistoryList(listHist);

        BankAccount result = testObj.accountMapped(bankAccountDto);
        assertThat(result).isEqualToIgnoringGivenFields(bankAccountDto, "id", "customer");
    }

}
