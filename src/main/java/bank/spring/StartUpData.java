package bank.spring;

import bank.spring.model.Customer.Customer;
import bank.spring.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class StartUpData implements ApplicationRunner {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    public StartUpData(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public void run(ApplicationArguments args) {
//        Customer sadmin = new Customer();
//        sadmin.setFirstName("super_admin");
//        sadmin.setLastName("super_admin");
//        sadmin.setEmail("superadmin@superadmin.pl");
//        sadmin.setUserName("sadmin");
//        sadmin.setPassword("sadmin");
//        sadmin.setRole("SUPERADMIN");
//        customerRepository.save(sadmin);

        Customer admin = new Customer();
        admin.setFirstName("admin");
        admin.setLastName("admin");
        admin.setEmail("admin@admin.pl");
        admin.setUserName("admin");
        admin.setPassword("admin");
        admin.setRole("ADMIN");
        customerRepository.save(admin);
    }
}
