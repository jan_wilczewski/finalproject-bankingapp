package bank.spring.Mappers.BankAccount;


import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.BankAccount.NewBankAccountDto;
import org.springframework.stereotype.Component;

@Component
public class BankAccountMapper3 {

    public BankAccount accountMapped (NewBankAccountDto newBankAccountDto){
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(newBankAccountDto.getBalance());
        bankAccount.setAccountNumber(newBankAccountDto.getAccountNumber());
        return bankAccount;

    }

    public NewBankAccountDto newBankAccountDto (BankAccount bankAccount){
        NewBankAccountDto newBankAccountDto = new NewBankAccountDto();
        newBankAccountDto.setBalance(bankAccount.getBalance());
        newBankAccountDto.setAccountNumber(bankAccount.getAccountNumber());
        return newBankAccountDto;
    }
}
