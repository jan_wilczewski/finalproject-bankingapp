package bank.spring.serviceImplement;

import bank.spring.Mappers.Customer.NewCustomerMapper;
import bank.spring.model.Customer.Customer;
import bank.spring.model.Customer.NewCustomerDto;
import bank.spring.repository.CustomerRepository;
import bank.spring.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.ws.http.HTTPException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService{

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private NewCustomerMapper newCustomerMapper;

    @Override
    public Customer findCustomerByUserName(String userName) {
        Customer customer = customerRepository.findCustomerByUserName(userName);
        if (customer == null) {
            throw new HTTPException(404);
        }
        else if (customer.getAddresses() == null
                || customer.getBankAccount() == null){
            throw new HTTPException(204);
        }
        return customer;
    }

    @Override
    public Customer findCustomerByAccountNumber(String accountNumber) {
        Customer customer = customerRepository.findCustomerByAccountNumber(accountNumber);
        if (customer == null){
            throw new HTTPException(404);
        }
        return customer;
    }


    public void addCustomerToDatabase(Customer customer){
        if (customer == null){
            throw new HTTPException(400);
        }
        customerRepository.save(customer);
    }


    @Override
    @Transactional
    public void removeCustomerFromDatabaseByAccountNumber(String accountNumber) {
        if (customerRepository.findCustomerByAccountNumber(accountNumber) == null){
            throw new HTTPException(404);
        }
        customerRepository.removeCustomerByAccountNumber(accountNumber);

    }


    @Override
    public boolean update(String customerName, String modification, String newValue) {
        Customer originalCustomer = customerRepository.findCustomerByUserName(customerName);

        if (originalCustomer == null){
            throw new HTTPException(400);
        }

        if (originalCustomer != null){
            switch (modification) {
                case ("username"):
                    originalCustomer.setUserName(newValue);
                    customerRepository.save(originalCustomer);
                    return true;
                case ("password"):
                    originalCustomer.setPassword(newValue);
                    customerRepository.save(originalCustomer);
                    return true;
                case ("firstname"):
                    originalCustomer.setFirstName(newValue);
                    customerRepository.save(originalCustomer);
                    return true;
                case ("lastname"):
                    originalCustomer.setLastName(newValue);
                    customerRepository.save(originalCustomer);
                    return true;
                case ("email"):
                    originalCustomer.setEmail(newValue);
                    customerRepository.save(originalCustomer);
                    return true;
                case ("phone"):
                    originalCustomer.setPhone(Integer.parseInt(newValue));
                    customerRepository.save(originalCustomer);
                    return true;
            }
        }
        return false;
    }

    @Override
    public List<NewCustomerDto> findCustomerByPassword(String password) {
        List<Customer> listCustomers = customerRepository.findCustomerByPassword(password);
        if (listCustomers.isEmpty()){
            throw new HTTPException(404);
        }
        List<NewCustomerDto> listNewCustomersDto = new ArrayList<>();
        for (Customer c: listCustomers){
            listNewCustomersDto.add(newCustomerMapper.newCustomerDtoMapped(c));
        }
        return listNewCustomersDto;
    }

    @Override
    public Customer findCustomer(String username) {
        return customerRepository.findCustomerByUserName(username);
    }



    private Collection<GrantedAuthority> mapRoles(Customer customer) {
        List<GrantedAuthority> roles = new ArrayList<>();
        if(customer.getRole().equals("ADMIN")){
            roles.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }else{
            roles.add(new SimpleGrantedAuthority("ROLE_USER"));
        }
        return roles;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Customer customer = findCustomer(userName);
        if (customer == null) {
            throw new HTTPException(404);
        }
        return new org.springframework.security.core.userdetails.User(customer.getUserName(), customer.getPassword(), true, true, true, true, mapRoles(customer));
    }


    //    @Override
//    public void register(Customer customer) {
//        if (customer.getUserName().toLowerCase().equals("admin")
//                || customer.getUserName().toLowerCase().equals("sadmin")){
//            throw new HTTPException(406);
//        }
//        customerRepository.save(customer);
//    }

//    @Override
//    public Customer validateUser(LoginDto login) {
//        return customerRepository.findCustomerByUsername(login.getUserName());
//    }
}
