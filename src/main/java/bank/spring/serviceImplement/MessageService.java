package bank.spring.serviceImplement;


import bank.spring.model.Customer.Customer;
import bank.spring.model.Message.Message;
import bank.spring.model.Message.MessageType;
import bank.spring.repository.CustomerRepository;
import bank.spring.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.http.HTTPException;
import java.sql.Date;
import java.util.List;

@Service
public class MessageService {


    @Autowired
    MessageRepository messageRepository;
    @Autowired
    CustomerRepository customerRepository;

    private Message createMessage(MessageType messageType, String text){
        Message message = new Message();
        message.setDate(new Date(System.currentTimeMillis()));
        message.setMessageType(messageType);
        message.setText(text);
        return message;
    }

    public boolean sendMessage(String userName, MessageType messageType, String text){
        Customer customer = customerRepository.findCustomerByUserName(userName);
        Message message = createMessage(messageType,text);

        if (customer == null){
            throw new HTTPException(404);
        }
        if (message == null || text.isEmpty()){
            throw new HTTPException(400);
        }
        List<Message>list = customer.getMessages();
        list.add(message);
        customer.setMessages(list);
        customerRepository.save(customer);
        return true;

    }

    public List<Message> showMessages(String userName){
        Customer customer = customerRepository.findCustomerByUserName(userName);
        if (customer == null){
            throw new HTTPException(404);
        }
        List<Message> list = customer.getMessages();
        return list;
    }


}
