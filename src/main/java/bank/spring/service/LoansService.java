package bank.spring.service;

import java.math.BigDecimal;

public interface LoansService {

    BigDecimal getInterestYearly(double principal, double rate, int time);
    BigDecimal getInterestMonthly(double principal, double rate, int time);
}
