package bank.spring.Mappers.Loan;

import bank.spring.model.Loans.Loan;
import bank.spring.model.Loans.LoanDto;
import bank.spring.model.Loans.LoanType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class LoanMapperTest {

    @InjectMocks
    private LoanMapper testObj = new LoanMapper();

    @Test
    public void isLoanDtoMapped(){
        Loan loan = new Loan();
        loan.setType(LoanType.CAR_LOAN);
        loan.setTime(2);
        loan.setRequestedValue(100);

        LoanDto result = testObj.loanDto(loan);
        assertThat(result).isEqualToIgnoringGivenFields(loan, "id");
    }

    @Test
    public void isLoanMapped(){
        LoanDto loanDto = new LoanDto();
        loanDto.setType(LoanType.CAR_LOAN);
        loanDto.setTime(2);
        loanDto.setRequestedValue(100);

        Loan result = testObj.loan(loanDto);
        assertThat(result).isEqualToIgnoringGivenFields(loanDto, "id");
    }
}
