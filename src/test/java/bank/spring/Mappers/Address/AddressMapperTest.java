package bank.spring.Mappers.Address;


import bank.spring.model.Address.Address;
import bank.spring.model.Address.AddressDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class AddressMapperTest {

    @InjectMocks
    private AddressMapper testObj = new AddressMapper();

    @Test
    public void isAddressDtoMappedCorrectly(){
        Address address = new Address();
        address.setStreet("aaa");
        address.setBuildingNumber(11);
        address.setAppartmentNumber(22);
        address.setZipCode("ddd");
        address.setCity("eee");
        address.setAccountNumber("fff");

        AddressDto result = testObj.addressDtoMapped(address);
        assertThat(result).isEqualToComparingFieldByField(address);

    }

    @Test
    public void isAddressMappedCorrectly(){
        AddressDto addressDto = new AddressDto();
        addressDto.setStreet("aaa");
        addressDto.setBuildingNumber(11);
        addressDto.setAppartmentNumber(22);
        addressDto.setZipCode("ddd");
        addressDto.setCity("eee");
        addressDto.setAccountNumber("fff");

        Address result = testObj.addressMapped(addressDto);
        assertThat(result).isEqualToIgnoringGivenFields(addressDto,"id");

    }

}
