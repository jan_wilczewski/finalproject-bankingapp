package bank.spring.repository;

import bank.spring.model.Address.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

    Set<Address> findByAccountNumber(String accountNumber);
    Address findByStreet(String street);
}
