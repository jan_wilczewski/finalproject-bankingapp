package bank.spring.controller;

import bank.spring.Mappers.Customer.CustomerMapper;
import bank.spring.Mappers.Customer.CustomerMapper4;
import bank.spring.Mappers.Customer.NewCustomerMapper;
import bank.spring.model.Customer.Customer;
import bank.spring.model.Customer.NewCustomerDto;
import bank.spring.service.CustomerService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.xml.ws.http.HTTPException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class CustomerControllerTest {

    @Mock
    public CustomerService customerService;
    @Mock
    private CustomerMapper customerMapper;
    @Mock
    private CustomerMapper4 customerMapper4;
    @Mock
    private NewCustomerMapper newCustomerMapper;
    @InjectMocks
    private CustomerController testObj = new CustomerController();

    @Test
    public void addCustomerTest(){
        NewCustomerDto newCustomerDto = new NewCustomerDto();
        Customer customer = new Customer();

        when(newCustomerMapper.newCustomerMapped(newCustomerDto)).thenReturn(customer);
        ResponseEntity<Void> result = testObj.addCustomerToDatabase(newCustomerDto);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(newCustomerMapper).newCustomerMapped(newCustomerDto);
        verify(customerService).addCustomerToDatabase(customer);
        verifyNoMoreInteractions(newCustomerMapper, customerService);
    }

    @Test
    public void addCustomerTest_Error(){
        NewCustomerDto newCustomerDto = new NewCustomerDto();
        Customer customer = new Customer();

        when(newCustomerMapper.newCustomerMapped(newCustomerDto)).thenReturn(customer);
        Mockito.doThrow(new HTTPException(400)).when(customerService).addCustomerToDatabase(customer);

        ResponseEntity<Void> result = testObj.addCustomerToDatabase(newCustomerDto);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        verify(newCustomerMapper).newCustomerMapped(newCustomerDto);
        verify(customerService).addCustomerToDatabase(customer);
        verifyNoMoreInteractions(newCustomerMapper, customerService);
    }

    @Test
    public void findByPasswordTest(){
        String password = "111";
        List<NewCustomerDto> list = new ArrayList<>();

        NewCustomerDto customer = new NewCustomerDto();
        list.add(customer);

        when(customerService.findCustomerByPassword("111")).thenReturn(list);

        ResponseEntity<List<NewCustomerDto>> result = testObj.findByPassword(password);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(list);
        assertThat(result.getBody()).containsExactly(customer);
        assertThat(result.getBody()).containsOnly(customer);
        verify(customerService).findCustomerByPassword(password);
        verifyNoMoreInteractions(customerService);

    }

    @Test
    public void findByPasswordTest_Error() {
        String password = "111";
        List<NewCustomerDto> list = new ArrayList<>();

        NewCustomerDto customer = new NewCustomerDto();
        list.add(customer);

        Mockito.doThrow(new HTTPException(404)).when(customerService).findCustomerByPassword("111");
        ResponseEntity<List<NewCustomerDto>> result = testObj.findByPassword(password);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(customerService).findCustomerByPassword(password);
        verifyNoMoreInteractions(customerService);
    }

    @Test
    public void removeCustomerTest_Error(){
        String accountNumber = "001";
        Customer customer = new Customer();

        Mockito.doThrow(new HTTPException(404)).when(customerService).removeCustomerFromDatabaseByAccountNumber("001");
        ResponseEntity<Void> result = testObj.removeCustomer("001");

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(customerService).removeCustomerFromDatabaseByAccountNumber(accountNumber);
        verifyNoMoreInteractions(customerService);
    }

    @Test
    public void addToCustomerTest(){
        String customerName = "aaa";
        String modification = "lastname";
        String newValue = "XXX";

        when(customerService.update(customerName, modification, newValue)).thenReturn(true);

        ResponseEntity result = testObj.addToCustomer(customerName, modification, newValue);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(customerService).update(customerName, modification, newValue);
        verifyNoMoreInteractions(customerService);
    }

    @Test
    public void addToCustomerTest_Error(){
        String customerName = "aaa";
        String modification = "lastname";
        String newValue = "XXX";

        when(customerService.update(customerName, modification, newValue)).thenReturn(false);

        ResponseEntity result = testObj.addToCustomer(customerName, modification, newValue);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        verify(customerService).update(customerName, modification, newValue);
        verifyNoMoreInteractions(customerService);
    }
}
