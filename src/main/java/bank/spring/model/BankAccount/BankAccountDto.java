package bank.spring.model.BankAccount;


import bank.spring.model.History.History;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

@ApiModel(description = "Bank Account")
public class BankAccountDto {

    @ApiModelProperty(value = "Balance")
    @JsonProperty("balance")
    private double balance;

    @ApiModelProperty(value = "Account Number")
    @JsonProperty("accountNumber")
    private String accountNumber;

    @ApiModelProperty(value = "History List")
    @JsonProperty("historyList")
    private List<History> historyList;

    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
    public String getAccountNumber() {
        return accountNumber;
    }
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public List<History> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<History> historyList) {
        this.historyList = historyList;
    }
}
