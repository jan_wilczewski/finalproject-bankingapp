package bank.spring.model.Loans;

/**
 * Created by jan_w on 06.10.2017.
 */
public enum LoanType {

    QUICK_CASH_LOAN(10.0),
    LONG_CASH_LOAN(7.5),
    CAR_LOAN(5.0),
    MORTGAGE(2.5);

    public double interestRate;

    LoanType(double interestRate) {
        this.interestRate = interestRate;
    }

    public double getInterestRate() {
        return interestRate;
    }
}
