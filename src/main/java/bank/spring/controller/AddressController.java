package bank.spring.controller;

import bank.spring.Mappers.Address.AddressMapper;
import bank.spring.model.Address.Address;
import bank.spring.model.Address.AddressDto;
import bank.spring.repository.CustomerRepository;
import bank.spring.service.AddressService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.http.HTTPException;
import java.util.List;
import java.util.Set;

@RestController
public class AddressController {

    @Autowired
    public AddressService addressService;
    @Autowired
    private AddressMapper addressMapper;
    @Autowired
    private CustomerRepository customerRepository;

    @RequestMapping(value = "/Address", method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE}, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Zapisuje adres klienta do bazy danych.", notes = "", response = Void.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Adres został zapisany w bazie.", response = Void.class),
            @ApiResponse(code = 400, message = "Niepoprawne dane wejściowe", response = Void.class),
            @ApiResponse(code = 500, message = "Nie można dodać adresu do nie założonego profilu klienta.", response = Void.class)})
    public ResponseEntity<Void> addAddressToDatabase(@ApiParam(value = "Rejestrowany user") @RequestBody AddressDto addressDto) {

        try {
            addressService.addAddressToDatabase(addressMapper.addressMapped(addressDto), addressDto.getAccountNumber());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (HTTPException e) {
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }

    @RequestMapping(value = "/AddressByAccountNumber", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Szuka adresu po numerze konta", notes = "", response = List.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Adres znaleziony", response = List.class),
            @ApiResponse(code = 401, message = "Nie znaleziono adresu", response = Void.class)})
    public ResponseEntity<Set<Address>> showAddresses(@ApiParam(value = "Podaj numer konta:") @RequestParam("Account Number") String accountNumber) {

        try {
            Set<Address> addresses = addressService.findAddressByAccountNumber(accountNumber);
            return new ResponseEntity(addresses, HttpStatus.OK);

        }catch (HTTPException e) {
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }

    @RequestMapping(value = "/Address", method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Zmienia wybrane dane adresowe.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Adres zaktualizowany", response = Void.class),
            @ApiResponse(code = 404, message = "Nie znaleziono adresu", response = Void.class) })
    public ResponseEntity<Void> addToAddress(@ApiParam(value = "Nazwa ulicy") @RequestParam("Street name") String street,
                                             @ApiParam(value = "Modyfikowane pole") @RequestParam("Modified variable") String modification,
                                             @ApiParam(value = "Nowa wartość") @RequestParam("New value") String newValue){

        boolean modified = addressService.update(street, modification, newValue);
        if (modified){
            return new ResponseEntity(modified, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    //    @RequestMapping(value = "/Address", method = RequestMethod.DELETE, produces = {
//            MediaType.APPLICATION_JSON_VALUE}, consumes = MediaType.APPLICATION_JSON_VALUE)
//    @ApiOperation(value = "Usuwa adres z bazy danych.", notes = "", response = Void.class)
//    @ApiResponses(value = {@ApiResponse(code = 200, message = "Adres został usunięty z bazy.", response = Void.class),
//            @ApiResponse(code = 400, message = "Nie ma w bazie takiego adresu", response = Void.class)})
//    public ResponseEntity<Void> removeAddressFromDatabaseByUsername(@ApiParam(value = "Szukana nazwa użytkownika") @RequestBody String accountNumber) {
//
//
////        if (addressService.findAddressByAccountNumber(accountNumber) != null) {
////            try {
////                addressService.removeAddressByAccountNumber(accountNumber);
////                return new ResponseEntity<>(HttpStatus.OK);
////            } catch (HTTPException e) {
////                return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
////            }
////        }
////        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
////    }
//
//        if (addressService.findAddressByAccountNumber(accountNumber) != null) {
//            addressService.removeAddressByAccountNumber(accountNumber);
//            return new ResponseEntity<>(HttpStatus.OK);
//        }
//        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//    }
}
