package bank.spring.controller;

import bank.spring.Mappers.Loan.LoanMapper;
import bank.spring.model.Loans.LoanType;
import bank.spring.service.LoansService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.http.HTTPException;
import java.math.BigDecimal;
import java.math.RoundingMode;

@RestController
public class LoanController {

    @Autowired
    LoansService loansService;
    @Autowired
    LoanMapper loanMapper;

    @RequestMapping(value = "/calculateLoanTotalCost", method = RequestMethod.PUT, consumes= MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Obliczenie rocznego kosztu kredytu.", notes = "", response = Double.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Wartość pożyczki obliczona", response = Double.class),
            @ApiResponse(code = 404, message = "Nie udało się obliczyć pożyczki", response = Double.class) })
    public ResponseEntity<BigDecimal> calculateTotalCost(@ApiParam(value = "Potrzebna kwota w PLN") @RequestParam("Requested value") double principal,
                                                       @ApiParam(value = "Rodzaj pożyczki") @RequestParam("Loan type") LoanType loanType,
                                                       @ApiParam(value = "Długość okresu pożyczki w latach") @RequestParam("Number of years") int time) {

        try {
            BigDecimal capitalizedSum = loansService.getInterestYearly(principal, loanType.getInterestRate(), time);
            return new ResponseEntity<BigDecimal>(capitalizedSum, HttpStatus.OK);
        }catch (HTTPException e) {
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }

    @RequestMapping(value = "/calculateMonhlyRate", method = RequestMethod.PUT, consumes= MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Obliczenie miesięcznej raty.", notes = "", response = Double.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Wartość pożyczki obliczona", response = Double.class),
            @ApiResponse(code = 404, message = "Nie udało się obliczyć pożyczki", response = Double.class) })
    public ResponseEntity<BigDecimal> calculateLoanMonthly(@ApiParam(value = "Potrzebna kwota w PLN") @RequestParam("Requested value") double principal,
                                                         @ApiParam(value = "Rodzaj pożyczki") @RequestParam("Loan type") LoanType loanType,
                                                         @ApiParam(value = "Długość okresu pożyczki w latach") @RequestParam("Number of years") int time) {

        try {
            BigDecimal capitalizedSum = loansService.getInterestMonthly(principal, loanType.getInterestRate(),time);
            return new ResponseEntity<BigDecimal>(capitalizedSum, HttpStatus.OK);
        }catch (HTTPException e) {
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }
}
