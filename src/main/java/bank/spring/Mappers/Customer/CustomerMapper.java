package bank.spring.Mappers.Customer;

import bank.spring.model.Customer.Customer;
import bank.spring.model.Customer.CustomerDto;
import org.springframework.stereotype.Component;

@Component
public class CustomerMapper {

    public Customer customerMapped (CustomerDto customerDto){
        Customer customer = new Customer();
        customer.setUserName(customerDto.getUserName());
        customer.setPassword(customerDto.getPassword());
        customer.setFirstName(customerDto.getFirstName());
        customer.setLastName(customerDto.getLastName());
        customer.setEmail(customerDto.getEmail());
        customer.setPhone(customerDto.getPhone());
        customer.setAccountNumber(customerDto.getAccountNumber());
        return customer;
    }

    public CustomerDto customerDtoMapped (Customer customer){
        CustomerDto customerDto = new CustomerDto();
        customerDto.setUserName(customer.getUserName());
        customerDto.setPassword(customer.getPassword());
        customerDto.setFirstName(customer.getFirstName());
        customerDto.setLastName(customer.getLastName());
        customerDto.setEmail(customer.getEmail());
        customerDto.setPhone(customer.getPhone());
        customerDto.setAccountNumber(customer.getAccountNumber());
        return customerDto;
    }


}
