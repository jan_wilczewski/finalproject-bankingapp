package bank.spring.Mappers.Loan;

import bank.spring.model.Loans.Loan;
import bank.spring.model.Loans.LoanDto;
import org.springframework.stereotype.Component;

@Component
public class LoanMapper {

    public Loan loan(LoanDto loanDto){
        Loan loan = new Loan();
        loan.setRequestedValue(loanDto.getRequestedValue());
        loan.setTime(loanDto.getTime());
        loan.setType(loanDto.getType());
        return loan;
    }

    public LoanDto loanDto(Loan loan){
        LoanDto loanDto = new LoanDto();
        loanDto.setRequestedValue(loan.getRequestedValue());
        loanDto.setTime(loan.getTime());
        loanDto.setType(loan.getType());
        return loanDto;
    }
}
