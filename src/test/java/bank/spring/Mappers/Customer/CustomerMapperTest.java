package bank.spring.Mappers.Customer;

import bank.spring.model.Customer.Customer;
import bank.spring.model.Customer.CustomerDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class CustomerMapperTest {

    @InjectMocks
    private CustomerMapper testObj = new CustomerMapper();

    @Test
    public void isCustomerDtoMappedCorrectly(){
        Customer customer = new Customer();
        customer.setUserName("aaa");
        customer.setPassword("bbb");
        customer.setFirstName("ccc");
        customer.setLastName("ddd");
        customer.setEmail("eee");
        customer.setPhone(123456789);
        customer.setAccountNumber("ggg");

        CustomerDto result = testObj.customerDtoMapped(customer);
        assertThat(result).isEqualToIgnoringGivenFields(customer, "id", "bankAccountDto", "addresses", "messages");

    }

    @Test
    public void isCustomerMappedCorrectly(){
        CustomerDto customerDto = new CustomerDto();
        customerDto.setUserName("aaa");
        customerDto.setPassword("bbb");
        customerDto.setFirstName("ccc");
        customerDto.setLastName("ddd");
        customerDto.setEmail("eee");
        customerDto.setPhone(123456789);
        customerDto.setAccountNumber("ggg");

        Customer result = testObj.customerMapped(customerDto);
        assertThat(result).isEqualToIgnoringGivenFields(customerDto, "id", "role", "bankAccount", "addresses", "messages");

    }

}
