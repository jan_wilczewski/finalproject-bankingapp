package bank.spring.repository;

import bank.spring.model.Customer.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {


    Customer findCustomerByUserName(String userName);
    List<Customer> findCustomerByPassword(String password);
    void removeCustomerByUserName(String userName);
    Customer findCustomerByAccountNumber(String accountNumber);
    void removeCustomerByAccountNumber(String accountNumber);


}
