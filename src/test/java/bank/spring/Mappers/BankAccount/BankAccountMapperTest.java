package bank.spring.Mappers.BankAccount;

import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.BankAccount.BankAccountDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class BankAccountMapperTest {

    @InjectMocks
    private BankAccountMapper testObj = new BankAccountMapper();

    @Test
    public void isAccountDtoMappedCorrectly(){
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(100);

        BankAccountDto result = testObj.accountDtoMapped(bankAccount);
        assertThat(result.getBalance()).isEqualTo(bankAccount.getBalance());
    }

    @Test
    public void  isAccountMappedCorrectly(){
        BankAccountDto bankAccountDto = new BankAccountDto();
        bankAccountDto.setBalance(100);

        BankAccount result = testObj.accountMapped(bankAccountDto);
        assertThat(result.getBalance()).isEqualTo(bankAccountDto.getBalance());
    }
}
