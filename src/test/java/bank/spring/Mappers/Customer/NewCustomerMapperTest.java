package bank.spring.Mappers.Customer;

import bank.spring.model.Customer.Customer;
import bank.spring.model.Customer.CustomerDto;
import bank.spring.model.Customer.NewCustomerDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class NewCustomerMapperTest {


    @InjectMocks
    private NewCustomerMapper testObj = new NewCustomerMapper();

    @Test
    public void isNewCustomerDtoMappedCorrectly(){
        Customer customer = new Customer();
        customer.setUserName("aaa");
        customer.setPassword("bbb");
        customer.setFirstName("ccc");
        customer.setLastName("ddd");
        customer.setEmail("eee");
        customer.setPhone(111);
        customer.setAccountNumber("fff");

        NewCustomerDto result = testObj.newCustomerDtoMapped(customer);
        assertThat(result).isEqualToIgnoringGivenFields(customer, "id", "bankAccountDto", "addresses", "messages");
    }

    @Test
    public void isNewCustomerMappedCorrectly(){
        NewCustomerDto newCustomerDto = new NewCustomerDto();
        newCustomerDto.setUserName("aaa");
        newCustomerDto.setPassword("bbb");
        newCustomerDto.setFirstName("ccc");
        newCustomerDto.setLastName("ddd");
        newCustomerDto.setEmail("eee");
        newCustomerDto.setPhone(123456789);
        newCustomerDto.setAccountNumber("ggg");

        Customer result = testObj.newCustomerMapped(newCustomerDto);
        assertThat(result).isEqualToIgnoringGivenFields(newCustomerDto, "id", "role", "bankAccount", "addresses", "messages");

    }

}
