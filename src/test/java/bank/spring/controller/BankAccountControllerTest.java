package bank.spring.controller;

import bank.spring.Mappers.BankAccount.BankAccountMapper;
import bank.spring.Mappers.BankAccount.BankAccountMapper2;
import bank.spring.Mappers.BankAccount.BankAccountMapper3;
import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.BankAccount.BankAccountDto;
import bank.spring.model.BankAccount.NewBankAccountDto;
import bank.spring.model.Customer.Customer;
import bank.spring.model.History.History;
import bank.spring.repository.CustomerRepository;
import bank.spring.service.BankAccountService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.xml.ws.http.HTTPException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;



@RunWith(MockitoJUnitRunner.class)
public class BankAccountControllerTest {


    @Mock
    private BankAccountService bankAccountService;
    @Mock
    private BankAccountMapper bankAccountMapper;
    @Mock
    private BankAccountMapper2 bankAccountMapper2;
    @Mock
    private BankAccountMapper3 bankAccountMapper3;
    @Mock
    private CustomerRepository customerRepository;
    @InjectMocks
    private BankAccountController testObj = new BankAccountController();


    @Test
    public void addAccountToDatabaseTest(){

        NewBankAccountDto newBankAccountDto = new NewBankAccountDto();
        newBankAccountDto.setAccountNumber("123");
        BankAccount bankAccount = new BankAccount();

        when(bankAccountMapper3.accountMapped(newBankAccountDto)).thenReturn(bankAccount);

        ResponseEntity<Void> result = testObj.addAccountToDatabase(newBankAccountDto);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(bankAccountMapper3).accountMapped(newBankAccountDto);
        verify(bankAccountService).addAccountToDatabase(bankAccount, "123");
        verifyNoMoreInteractions(bankAccountMapper3, bankAccountService);
    }

    @Test
    public void addAccountToDatabaseTest_Error(){

        NewBankAccountDto newBankAccountDto = new NewBankAccountDto();
        newBankAccountDto.setAccountNumber("123");
        BankAccount bankAccount = new BankAccount();

        when(bankAccountMapper3.accountMapped(newBankAccountDto)).thenReturn(bankAccount);
        Mockito.doThrow(new HTTPException(404)).when(bankAccountService).addAccountToDatabase(bankAccount, "123");

        ResponseEntity<Void> result = testObj.addAccountToDatabase(newBankAccountDto);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(bankAccountMapper3).accountMapped(newBankAccountDto);
        verify(bankAccountService).addAccountToDatabase(bankAccount, "123");
        verifyNoMoreInteractions(bankAccountMapper3, bankAccountService);
    }

    @Test
    public void findByAccountNumberTest(){

        String accountNumber = "123";
        BankAccount bankAccount = new BankAccount();

        when(bankAccountService.findAccountByAccountNumber("123")).thenReturn(bankAccount);

        ResponseEntity<BankAccount> result = testObj.findAccountByAccountNumber(accountNumber);
        BankAccountDto bankAccountDto = bankAccountMapper2.accountDtoMapped(bankAccount);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(bankAccountDto);

        //verify(bankAccountMapper2).accountDtoMapped(bankAccount);
        verify(bankAccountService).findAccountByAccountNumber(accountNumber);
        //verifyNoMoreInteractions(bankAccountMapper2, bankAccountService);
    }

    @Test
    public void findByAccountNumberTest_Error(){

        String accountNumber = "123";
        BankAccount bankAccount = new BankAccount();

        Mockito.doThrow(new HTTPException(404)).when(bankAccountService).findAccountByAccountNumber("123");

        ResponseEntity<BankAccount> result = testObj.findAccountByAccountNumber(accountNumber);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        //verify(bankAccountMapper2).accountDtoMapped(bankAccount);
        verify(bankAccountService).findAccountByAccountNumber(accountNumber);
        //verifyNoMoreInteractions(bankAccountMapper2, bankAccountService);

    }


    @Test
    public void showHistoryTest(){

        String accountNumber = "123";
        List<History> list = new ArrayList<>();

        History history = new History();
        list.add(history);

        when(bankAccountService.showAccountHistoryByAccountNumber("123")).thenReturn(list);

        ResponseEntity<List<History>> result = testObj.showHistory(accountNumber);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(list);
        assertThat(result.getBody()).containsOnly(history);
        verify(bankAccountService).showAccountHistoryByAccountNumber(accountNumber);
        verifyNoMoreInteractions(bankAccountService);
    }

    @Test
    public void showHistoryTest_Error(){

        String accountNumber = "123";
        List<History> list = new ArrayList<>();

        History history = new History();
        list.add(history);

        Mockito.doThrow(new HTTPException(204)).when(bankAccountService).showAccountHistoryByAccountNumber("123");
        ResponseEntity<List<History>> result = testObj.showHistory(accountNumber);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        verify(bankAccountService).showAccountHistoryByAccountNumber(accountNumber);
        verifyNoMoreInteractions(bankAccountService);
    }


    @Test
    public void paymentOnAccountTest(){

        String accountNumber = "123";
        Double cash = 23.0;

        when(bankAccountService.paymentOnAccount("123", 23.0)).thenReturn(true);
        ResponseEntity<Void> result = testObj.paymentOnAccount("123", 23.0);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(bankAccountService).paymentOnAccount(accountNumber, cash);
        verifyNoMoreInteractions(bankAccountService);
    }

    @Test
    public void paymentOnAccountTest_Error(){

        String accountNumber = "123";
        Double cash = 23.0;

        Mockito.doThrow(new HTTPException(400)).when(bankAccountService).paymentOnAccount(accountNumber, cash);
        ResponseEntity<Void> result = testObj.paymentOnAccount(accountNumber, cash);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        verify(bankAccountService).paymentOnAccount(accountNumber, cash);
        verifyNoMoreInteractions(bankAccountService);
    }

    @Test
    public void withdrawFromAccountTest(){

        String accountNumber = "123";
        Double cash = 23.0;


        when(bankAccountService.withdrawFromAccount("123", 23.0)).thenReturn(true);
        ResponseEntity<Void> result = testObj.withdrawFromAccount("123", 23.0);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(bankAccountService).withdrawFromAccount(accountNumber, cash);
        verifyNoMoreInteractions(bankAccountService);
    }

    @Test
    public void withdrawFromAccountTest_Error(){

        String accountNumber = "123";
        Double cash = 23.0;

        Mockito.doThrow(new HTTPException(400)).when(bankAccountService).withdrawFromAccount(accountNumber, cash);
        ResponseEntity<Void> result = testObj.withdrawFromAccount(accountNumber, cash);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        verify(bankAccountService).withdrawFromAccount(accountNumber, cash);
        verifyNoMoreInteractions(bankAccountService);
    }

    @Test
    public void transferToOtherAccountTest(){

        String accountNumber = "123";
        Double cash = 23.0;
        String receiverAccount = "456";

        when(bankAccountService.transferToOtherAccount("123", 23.0, "456")).thenReturn(true);
        ResponseEntity<Void> result = testObj.transferToOtherAccount("123", 23.0, "456");
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(bankAccountService).transferToOtherAccount(accountNumber, cash, receiverAccount);
        verifyNoMoreInteractions(bankAccountService);
    }

    @Test
    public void transferToOtherAccountTest_Error(){

        String accountNumber = "123";
        Double cash = 23.0;
        String receiverAccount = "456";

        Mockito.doThrow(new HTTPException(400)).when(bankAccountService).transferToOtherAccount(accountNumber, cash, receiverAccount);
        ResponseEntity<Void> result = testObj.transferToOtherAccount(accountNumber, cash, receiverAccount);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        verify(bankAccountService).transferToOtherAccount(accountNumber, cash, receiverAccount);
        verifyNoMoreInteractions(bankAccountService);
    }




}
