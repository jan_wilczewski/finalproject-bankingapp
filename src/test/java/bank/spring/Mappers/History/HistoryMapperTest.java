package bank.spring.Mappers.History;

import bank.spring.model.History.History;
import bank.spring.model.History.HistoryDto;
import bank.spring.model.History.HistoryType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class HistoryMapperTest {

    @InjectMocks
    private HistoryMapper testObj = new HistoryMapper();

    @Test
    public void isHistoryDtoMapped(){

        History history = new History();
        history.setHistoryType(HistoryType.WITHDRAW);
        history.setText("aaa");
        history.setCash(20);
        history.setDate(new Date(System.currentTimeMillis()));

        HistoryDto result = testObj.historyDto(history);
        assertThat(result).isEqualToIgnoringGivenFields(history, "id");
    }

    @Test
    public void isHistoryMapped(){

        HistoryDto historyDto = new HistoryDto();
        historyDto.setHistoryType(HistoryType.WITHDRAW);
        historyDto.setText("aaa");
        historyDto.setCash(20);
        historyDto.setDate(new Date(System.currentTimeMillis()));

        History result = testObj.historyMapped(historyDto);
        assertThat(result).isEqualToIgnoringGivenFields(historyDto, "id");
    }


}
