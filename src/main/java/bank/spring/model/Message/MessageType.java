package bank.spring.model.Message;

public enum MessageType {
    NEWS("Nowości nowości!"), WARNING("Uwaga uwaga!"), PROMO("Promo promo!"), WISHES("Naj Naj Naj");

    public String text;

    MessageType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
