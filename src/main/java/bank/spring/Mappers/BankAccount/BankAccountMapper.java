package bank.spring.Mappers.BankAccount;

import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.BankAccount.BankAccountDto;
import org.springframework.stereotype.Component;

@Component
public class BankAccountMapper {

    public BankAccount accountMapped (BankAccountDto bankAccountDto){
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(bankAccountDto.getBalance());
        return bankAccount;

    }

    public BankAccountDto accountDtoMapped (BankAccount bankAccount){
        BankAccountDto bankAccountDto = new BankAccountDto();
        bankAccountDto.setBalance(bankAccount.getBalance());
        return bankAccountDto;
    }
}
