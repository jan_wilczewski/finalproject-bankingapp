package bank.spring.validator;

import bank.spring.model.Customer.Customer;
import bank.spring.model.Customer.CustomerDto;
import bank.spring.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    @Autowired
    CustomerService customerService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == CustomerDto.class;
    }

    @Override
    public void validate(Object obj, Errors errors) {
        if(obj instanceof CustomerDto){
            CustomerDto customer = (CustomerDto) obj;
            Customer found = customerService.findCustomer(customer.getUserName());
            if(found != null){
                errors.reject("login");
            }
        }

    }

}
