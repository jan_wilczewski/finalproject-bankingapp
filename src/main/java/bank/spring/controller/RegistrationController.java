//package bank.spring.controller;
//
//import bank.spring.Mappers.AdminMapper;
//import bank.spring.Mappers.CustToRegMapper;
//import bank.spring.Mappers.CustomerMapper;
//import bank.spring.model.AdminDto;
//import bank.spring.model.CustToRegDto;
//import bank.spring.model.CustomerDto;
//import bank.spring.service.AdminService;
//import bank.spring.service.CustomerService;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.xml.ws.http.HTTPException;
//
//@RestController
//public class RegistrationController {
//
//    @Autowired
//    public CustomerService customerService;
//    @Autowired
//    private CustToRegMapper custToRegMapper;
//
//    @RequestMapping(value = "/registerProcess", method = RequestMethod.POST, produces = {
//            MediaType.APPLICATION_JSON_VALUE },consumes=MediaType.APPLICATION_JSON_VALUE)
//    @ApiOperation(value = "Rejestruje usera.", notes = "", response = Void.class)
//    @ApiResponses(value = { @ApiResponse(code = 200, message = "User zarejestrowany", response = Void.class),
//            @ApiResponse(code = 400, message = "Niepoprawne dane wejściowe", response = Void.class),
//            @ApiResponse(code = 406, message = "Not accepted!", response = Void.class) })
//    public ResponseEntity<Void> addUser(@ApiParam(value = "Rejestrowany user") @RequestBody CustToRegDto custToRegDto) {
//
//        try {
//            customerService.register(custToRegMapper.custToRegMapped(custToRegDto));
//            return new ResponseEntity<>(HttpStatus.OK);
//        }catch (HTTPException e){
//            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
//        }
//    }
//}
