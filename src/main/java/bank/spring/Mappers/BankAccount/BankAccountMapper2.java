package bank.spring.Mappers.BankAccount;

import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.BankAccount.BankAccountDto;
import org.springframework.stereotype.Component;

@Component
public class BankAccountMapper2 {

    public BankAccount accountMapped (BankAccountDto bankAccountDto){
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(bankAccountDto.getBalance());
        bankAccount.setAccountNumber(bankAccountDto.getAccountNumber());
        bankAccount.setHistoryList(bankAccountDto.getHistoryList());
        return bankAccount;

    }

    public BankAccountDto accountDtoMapped (BankAccount bankAccount){
        BankAccountDto bankAccountDto = new BankAccountDto();
        bankAccountDto.setBalance(bankAccount.getBalance());
        bankAccountDto.setAccountNumber(bankAccount.getAccountNumber());
        bankAccountDto.setHistoryList(bankAccount.getHistoryList());
        return bankAccountDto;
    }
}
