package bank.spring.Mappers.Customer;

import bank.spring.model.BankAccount.BankAccount;
import bank.spring.model.BankAccount.BankAccountDto;
import bank.spring.model.Customer.Customer;
import bank.spring.model.Customer.CustomerDto;
import org.springframework.stereotype.Component;

@Component
public class CustomerMapper4 {

    public Customer customerMapped (CustomerDto customerDto){
        Customer customer = new Customer();
        BankAccount account = new BankAccount();

        customer.setUserName(customerDto.getUserName());
        customer.setPassword(customerDto.getPassword());
        customer.setFirstName(customerDto.getFirstName());
        customer.setLastName(customerDto.getLastName());
        customer.setEmail(customerDto.getEmail());
        customer.setPhone(customerDto.getPhone());
        customer.setAccountNumber(customerDto.getAccountNumber());
        customer.setMessages(customerDto.getMessages());
        customer.setAddresses(customerDto.getAddresses());

        account.setBalance(customerDto.getBankAccountDto().getBalance());
        account.setHistoryList(customerDto.getBankAccountDto().getHistoryList());
        customer.setBankAccount(account);

        return customer;
    }

    public CustomerDto customerDtoMapped (Customer customer){
        CustomerDto customerDto = new CustomerDto();
        BankAccountDto accountDto = new BankAccountDto();

        customerDto.setUserName(customer.getUserName());
        customerDto.setPassword(customer.getPassword());
        customerDto.setFirstName(customer.getFirstName());
        customerDto.setLastName(customer.getLastName());
        customerDto.setEmail(customer.getEmail());
        customerDto.setPhone(customer.getPhone());
        customerDto.setAccountNumber(customer.getAccountNumber());
        customerDto.setMessages(customer.getMessages());
        customerDto.setAddresses(customer.getAddresses());

        accountDto.setBalance(customer.getBankAccount().getBalance());
        accountDto.setHistoryList(customer.getBankAccount().getHistoryList());
        accountDto.setAccountNumber(customer.getAccountNumber());
        customerDto.setBankAccountDto(accountDto);

        return customerDto;
    }
}
