package bank.spring.controller;

import bank.spring.Mappers.Message.MessageMapper;
import bank.spring.model.Message.Message;
import bank.spring.model.Message.MessageType;
import bank.spring.serviceImplement.MessageService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.xml.ws.http.HTTPException;
import java.util.List;

@Controller
public class MessageController {

    @Autowired
    MessageService messageService;
    @Autowired
    MessageMapper messageMapper;



    @RequestMapping(value = "/sendMessage", method = RequestMethod.PUT, consumes= MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Wysłanie wiadomości", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Wiadomość wysłana", response = Void.class),
            @ApiResponse(code = 404, message = "Nie znaleziono konta", response = Void.class) })
    public ResponseEntity<Void> sendMessage(@ApiParam(value = "Nazwa użytkownika") @RequestParam("username") String username,
                                            @ApiParam(value = "Rodzaj wiadomości") @RequestParam("Message type") MessageType messageType,
                                            @ApiParam(value = "Treść") @RequestParam("text") String text) {

        try {
            boolean ifSent = messageService.sendMessage(username, messageType, text);
            return new ResponseEntity(HttpStatus.OK);
        }catch (HTTPException e) {
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }


    @RequestMapping(value = "/getMessages", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Wyświetlanie wiadomości", notes = "", response = List.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Wiadomości znalezione", response = List.class),
            @ApiResponse(code = 401, message = "Nie ma żadnych wiadomości", response = Void.class) })
    public ResponseEntity<List<Message>> showMessages(@ApiParam(value = "Skrzynka odbiorcza klienta") @RequestParam("username") String username) {

        try {
            List<Message> list = messageService.showMessages(username);
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch (HTTPException e) {
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }

}
