package bank.spring.controller;

import bank.spring.Mappers.Message.MessageMapper;
import bank.spring.model.Message.Message;
import bank.spring.model.Message.MessageType;
import bank.spring.serviceImplement.MessageService;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.xml.ws.http.HTTPException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class MessageControllerTest {

    @Mock
    MessageService messageService;
    @Mock
    MessageMapper messageMapper;
    @InjectMocks
    private MessageController testObj = new MessageController();

    @Test
    public void sendMessageTest(){

        String username = "aaa";
        String text = "xxxxx";
        MessageType messageType = MessageType.NEWS;

        when(messageService.sendMessage(username, messageType, text)).thenReturn(true);
        ResponseEntity<Void> result = testObj.sendMessage(username, messageType, text);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(messageService).sendMessage(username, messageType, text);
        verifyNoMoreInteractions(messageService);
    }

    @Test
    public void sendMessageTest_Error(){

        String username = "aaa";
        String text = "xxxxx";
        MessageType messageType = MessageType.NEWS;

        Mockito.doThrow(new HTTPException(400)).when(messageService).sendMessage(username, messageType, text);
        ResponseEntity<Void> result = testObj.sendMessage(username, messageType, text);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        verify(messageService).sendMessage(username, messageType, text);
        verifyNoMoreInteractions(messageService);
    }

    @Test
    public void showMessagesTest(){
        String username = "aaa";
        List<Message> list = new ArrayList<>();

        Message message = new Message();
        list.add(message);

        when(messageService.showMessages(username)).thenReturn(list);
        ResponseEntity<List<Message>> result = testObj.showMessages(username);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(list);
        assertThat(result.getBody()).containsOnly(message);
        verify(messageService).showMessages(username);
        verifyNoMoreInteractions(messageService);
    }

    @Test
    public void showMessagesTest_Eror() {
        String username = "aaa";
        List<Message> list = new ArrayList<>();

        Message message = new Message();
        list.add(message);

        Mockito.doThrow(new HTTPException(404)).when(messageService).showMessages(username);
        ResponseEntity<List<Message>> result = testObj.showMessages(username);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(messageService).showMessages(username);
        verifyNoMoreInteractions(messageService);

    }

}
