package bank.spring.Mappers.Message;

import bank.spring.model.Message.Message;
import bank.spring.model.Message.MessageDto;
import org.springframework.stereotype.Component;

@Component
public class MessageMapper {

    public Message messageMapped(MessageDto messageDto){
        Message message = new Message();
        message.setDate(messageDto.getDate());
        message.setMessageType(messageDto.getMessageType());
        message.setText(messageDto.getText());
//        message.setSpecialText(messageDto.getMessageType().toString());
        return message;
    }

    public MessageDto messageDto (Message message){
        MessageDto messageDto = new MessageDto();
        messageDto.setDate(message.getDate());
        messageDto.setMessageType(message.getMessageType());
        messageDto.setText(message.getText());
//        messageDto.setSpecialText(message.getMessageType().getText().toString());
        return messageDto;

    }
}
