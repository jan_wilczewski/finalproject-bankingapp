package bank.spring.model.Loans;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Enumerated;

@ApiModel(description = "Loans")
public class LoanDto {

    @ApiModelProperty(value = "Id")
    @JsonProperty("id")
    private Long id;
    @ApiModelProperty(value = "Requested Value")
    @JsonProperty("requestedValue")
    private double requestedValue;
    @ApiModelProperty(value = "Time")
    @JsonProperty("time")
    private int time;
    @ApiModelProperty(value = "Type")
    @JsonProperty("type")
    @Enumerated
    private LoanType type;

    public double getRequestedValue() {
        return requestedValue;
    }

    public void setRequestedValue(double requestedValue) {
        this.requestedValue = requestedValue;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public LoanType getType() {
        return type;
    }

    public void setType(LoanType type) {
        this.type = type;
    }


}
