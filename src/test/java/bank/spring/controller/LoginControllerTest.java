package bank.spring.controller;

import bank.spring.model.LoginDto;
import bank.spring.serviceImplement.TokenAuthenticationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoginControllerTest {

    @Mock
    private TokenAuthenticationService tokenService;

    @InjectMocks
    private LoginController testObj = new LoginController();


    @Test
    public void shouldReturn401StatusWhenTokenIsEmpty(){
        LoginDto loginDto = new LoginDto();
        when(tokenService.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()))).thenReturn(null);

        ResponseEntity<String> result = testObj.loginProcess(loginDto);

        assertThat(result.getStatusCodeValue()).isEqualTo(401);
        assertThat(result.getBody()).isNull();
        verify(tokenService).authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(),loginDto.getPassword()));
        verifyNoMoreInteractions(tokenService);

    }

    @Test
    public void shouldReturn200StatusWhenTokenIsNotEmpty(){
        String token = "username_password";
        LoginDto loginDto = new LoginDto();
        when(tokenService.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()))).thenReturn(token);

        ResponseEntity<String> result = testObj.loginProcess(loginDto);

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.getBody()).isEqualTo(token);
        verify(tokenService).authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(),loginDto.getPassword()));
        verifyNoMoreInteractions(tokenService);

    }

}
