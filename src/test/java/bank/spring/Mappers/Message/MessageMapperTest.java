package bank.spring.Mappers.Message;

import bank.spring.model.Message.Message;
import bank.spring.model.Message.MessageDto;
import bank.spring.model.Message.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class MessageMapperTest {

    @InjectMocks
    private MessageMapper testObj = new MessageMapper();

    @Test
    public void isMessageDtoMapped(){
        Message message = new Message();
        message.setText("aaa");
        message.setMessageType(MessageType.NEWS);
        message.setDate(new Date(System.currentTimeMillis()));

        MessageDto result = testObj.messageDto(message);
        assertThat(result).isEqualToIgnoringGivenFields(message, "id");
    }

    @Test
    public void isMessageMapped(){
        MessageDto messageDto = new MessageDto();
        messageDto.setText("aaa");
        messageDto.setMessageType(MessageType.NEWS);
        messageDto.setDate(new Date(System.currentTimeMillis()));

        Message result = testObj.messageMapped(messageDto);
        assertThat(result).isEqualToIgnoringGivenFields(messageDto, "id");
    }


}
